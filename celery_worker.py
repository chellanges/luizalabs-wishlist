from app import celery, create_app

app = create_app('development')
app.app_context().push()
