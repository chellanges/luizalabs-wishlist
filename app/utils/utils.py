def apply_filter(query, items):
    for field, value in items:
        query = query.filter_by(**{field: value})
    return query


def get_paginated_list(items, url, offset, limit):
    # check if page exists
    count = len(items)
    if count < offset:
        return []
    # make response
    obj = dict()
    obj['offset'] = offset
    obj['limit'] = limit
    obj['count'] = count
    # make URLs
    # make previous url
    if offset == 1:
        obj['previous'] = ''
    else:
        start_copy = max(1, offset - limit)
        limit_copy = offset - 1
        obj['previous'] = url + '?offset=%d&limit=%d' % (start_copy, limit_copy)
    # make next url
    if offset + limit > count:
        obj['next'] = ''
    else:
        start_copy = offset + limit
        obj['next'] = url + '?offset=%d&limit=%d' % (start_copy, limit)
    # finally extract result according to bounds
    obj['results'] = items[(offset - 1):(offset - 1 + limit)]
    return obj
