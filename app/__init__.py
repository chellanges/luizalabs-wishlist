from celery import Celery
from config.settings import config
from flask import Flask

from .ext import api, db, jwt, marshmallow

celery = Celery(__name__)


def create_app(config_name=None) -> Flask:
    app = Flask(__name__)

    if config_name is None:
        config_name = app.config["ENV"]

    config_obj = config[config_name]
    app.config.from_object(config_obj)
    config_obj.init_app(app)

    celery.conf.update(app.config)
    celery.config_from_object('app.jobs.celeryconfig')

    # Extensions
    api.init_app(app)
    db.init_app(app)
    marshmallow.init_app(app)
    jwt.init_app(app)

    # Blueprints
    from .blueprints.clients import clients_bp, clients_ns
    app.register_blueprint(clients_bp, url_prefix='/api/v1/clients')
    api.add_namespace(clients_ns)

    from .blueprints.wishlist import wishlist_bp, wishlist_ns
    app.register_blueprint(wishlist_bp, url_prefix='/api/v1/wishlist')
    api.add_namespace(wishlist_ns)

    from .blueprints.tasks import tasks as tasks_bp
    app.register_blueprint(tasks_bp, url_prefix='/api/v1/tasks')

    from .blueprints.auth import auth_bp, auth_ns
    app.register_blueprint(auth_bp, url_prefix='/api/v1/auth')
    api.add_namespace(auth_ns)

    return app
