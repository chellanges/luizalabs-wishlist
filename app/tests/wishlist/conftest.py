import pytest

from app.blueprints.wishlist.dao import WishlistDAO
from app.blueprints.wishlist.models import Wishlist


@pytest.fixture(scope='module')
def new_wishlist():
    wishlist = Wishlist(
        product_id='e6020eeb-74d3',
        title='Película Protetora',
        price=39.9,
        image='http://teste.com/images/e6020eeb-74d3.jpg',
        brand='elg',
        review_score=2.3333333,
        client_id=1
    )
    return wishlist


@pytest.fixture(scope='module')
def init_wishlist():
    # Insert wishlist data
    wishlist1 = {
        'product_id': 'e6020eeb-74d3',
        'title': 'Película Protetora',
        'price': 39.9,
        'image': 'http://teste.com/images/e6020eeb-74d3.jpg',
        'brand': 'elg',
        'review_score': 2.3333333,
        'client_id': 1
    }
    wishlist2 = {
        'product_id': 'e6020eeb-74d345',
        'title': 'Película Protetora Teste',
        'price': 38.12,
        'image': 'http://teste.com/images/e6020eeb-74d3.png',
        'brand': 'inovix',
        'review_score': 3.5433333,
        'client_id': 2
    }
    wishlistDAO = WishlistDAO()
    wishlistDAO.create(wishlist1)
    wishlistDAO.create(wishlist2)
