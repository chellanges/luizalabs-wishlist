def test_new_wishlist(new_wishlist):
    """
    GIVEN a Wishlist model
    WHEN a new Wishlist is created
    THEN check the properties are defined correctly
    """
    assert new_wishlist.product_id == 'e6020eeb-74d3'
    assert new_wishlist.title == 'Película Protetora'
    assert new_wishlist.price == 39.9
    assert new_wishlist.image == 'http://teste.com/images/e6020eeb-74d3.jpg'
    assert new_wishlist.client_id == 1
    assert new_wishlist.brand == 'elg'
    assert new_wishlist.reviewScore == 2.3333333
