import pytest

from flask_jwt_extended import create_access_token

from app import create_app
from app.ext.db import db

from app.blueprints.auth.dao import UserDAO


@pytest.fixture(scope='module')
def init_database():
    db.create_all()
    db.session.commit()

    # Insert user data
    userDAO = UserDAO()
    userDAO.create({'username': 'teste', 'password': '12345'})
    userDAO.create({'username': 'teste2', 'password': '123'})

    yield db

    db.drop_all()


@pytest.fixture(scope='module')
def test_client():
    flask_app = create_app('testing')

    testing_client = flask_app.test_client()

    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()


@pytest.fixture(scope='module')
def access_token(init_database):
    return create_access_token(identity='teste')
