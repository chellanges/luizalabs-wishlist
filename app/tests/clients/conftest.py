import pytest

from app.blueprints.clients.dao import ClientDAO
from app.blueprints.clients.models import Client


@pytest.fixture(scope='module')
def new_client():
    client = Client('FlaskIsAwesome', 'test@email.com')
    return client


@pytest.fixture(scope='module')
def init_clients():
    # Insert client data
    clientDAO = ClientDAO()
    clientDAO.create({'name': 'teste5', 'email': 'teste@teste.com'})
    clientDAO.create({'name': 'teste6', 'email': 'teste@teste.com.br'})
