from flask import json


def test_get_all(test_client, access_token):
    """
    GIVEN a Flask application
    WHEN the '/api/v1/clients' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get(
        '/api/v1/clients/',
        headers=dict(Authorization="Bearer " + access_token))
    assert response.status_code == 200


def test_get_single(test_client, access_token, init_clients):
    """
    GIVEN a Flask application
    WHEN the '/api/v1/clients/<pk>' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/api/v1/clients/1',
                               headers=dict(Authorization="Bearer " + access_token))
    data = json.loads(response.get_data(as_text=True))

    assert response.status_code == 200
    assert data['name'] == 'teste5'
    assert data['email'] == 'teste@teste.com'


def test_add(test_client, access_token):
    """
    GIVEN a Flask application
    WHEN the '/api/v1/clients' page is requested (POST)
    THEN check the response is valid
    """
    response = test_client.post(
        '/api/v1/clients/',
        data=json.dumps({'name': 'More Teste',
                         'email': 'teste@example.com'}),
        content_type='application/json',
        headers=dict(Authorization="Bearer " + access_token)
    )
    data = json.loads(response.get_data(as_text=True))

    assert response.status_code == 201
    assert data['name'] == 'More Teste'
    assert data['email'] == 'teste@example.com'


def test_update(test_client, access_token):
    """
    GIVEN a Flask application
    WHEN the '/api/v1/clients/<pk>' page is requested (PUT)
    THEN check the response is valid
    """
    response = test_client.put(
        '/api/v1/clients/1',
        data=json.dumps({'name': 'More Teste',
                         'email': 'teste@teste.com'}),
        content_type='application/json',
        headers=dict(Authorization="Bearer " + access_token)
    )
    data = json.loads(response.get_data(as_text=True))

    assert response.status_code == 200
    assert data['name'] == 'More Teste'
    assert data['email'] == 'teste@teste.com'


def test_delete(test_client, access_token, init_clients):
    """
    GIVEN a Flask application
    WHEN the '/api/v1/clients/<pk>' page is requested (DELETE)
    THEN check the response is valid
    """
    response = test_client.delete('/api/v1/clients/3',
                                  headers=dict(Authorization="Bearer " + access_token))

    assert response.status_code == 204
