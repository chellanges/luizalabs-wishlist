def test_new_client(new_client):
    """
    GIVEN a Client model
    WHEN a new Client is created
    THEN check the name and email are defined correctly
    """
    assert new_client.name == 'FlaskIsAwesome'
    assert new_client.email == 'test@email.com'
