from app.ext.marshmallow import ma

from .models import Wishlist


class WishlistSchema(ma.ModelSchema):
    class Meta:
        model = Wishlist
        fields = ('id', 'client_id', 'product_id', 'title', 'price',
                  'image', 'brand', 'reviewScore', '_links', )

    _links = ma.Hyperlinks({
        'self': ma.URLFor('wishlist_wishlist', pk='<id>'),
        'collection': ma.URLFor('wishlist_wishlist_list'),
        'client': ma.URLFor('clients_client', pk='<client_id>'),
    })


wishlist_schema = WishlistSchema()
wishlists_schema = WishlistSchema(many=True)
