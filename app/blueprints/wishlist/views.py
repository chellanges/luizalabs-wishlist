from flask import current_app as app, jsonify, request, url_for
from flask_jwt_extended import jwt_required
from flask_restplus import Resource

from app.utils.utils import get_paginated_list

from app.jobs.tasks import add_product_list as tk_add_product_list
from app.jobs.tasks import delete_wishlist as tk_delete_wishlist

from . import wishlist_ns, wishlist_list_md
from .dao import WishlistDAO
from .schemas import wishlist_schema, wishlists_schema

DAO = WishlistDAO()


@wishlist_ns.route('/')
@wishlist_ns.doc(security='apiKey')
@wishlist_ns.response(401, 'Missing Authorization Header')
class WishlistList(Resource):
    """Shows a list of all wishlists, and lets you POST to add new wishlist"""

    decorators = [jwt_required]

    @wishlist_ns.doc('list_wishlist')
    @wishlist_ns.param('offset', 'Offset items')
    @wishlist_ns.param('limit', 'Limit items')
    @wishlist_ns.param('client_id', 'Wishlist by client_id')
    @wishlist_ns.response(200, 'Successful')
    def get(self):
        """List all wishlists"""
        args = request.args.to_dict(flat=True)
        start = int(args.pop('offset', 1))
        limit = int(args.pop('limit', app.config['ITEMS_PER_PAGE']))
        result = wishlists_schema.dump(DAO.list(args))
        result_paginated = get_paginated_list(
            result, url_for('wishlist_wishlist_list'), start, limit)
        return jsonify(result_paginated)

    @wishlist_ns.doc('creste_wishlist')
    @wishlist_ns.expect(wishlist_list_md)
    @wishlist_ns.response(202, 'Added task in background to create wishlist')
    @wishlist_ns.response(400, 'Missing or incorrect fields')
    def post(self):
        """Add task to create a new wishlist"""
        task = tk_add_product_list.apply_async([wishlist_ns.payload])
        location = {'Location': url_for('tasks.status', task_id=task.id)}
        return '', 202, location

    @wishlist_ns.doc('delete_wishlist_by_client')
    @wishlist_ns.param('client_id', 'The client identifier', required=True)
    @wishlist_ns.response(202, 'Added task background to delete wishlist')
    def delete(self):
        """Delete a wishlist given the client identifier"""
        client_id = request.args['client_id']
        task = tk_delete_wishlist.apply_async([client_id])
        location = {'Location': url_for('tasks.status', task_id=task.id)}
        return '', 202, location


@wishlist_ns.route('/<int:pk>')
@wishlist_ns.doc(security='apiKey')
@wishlist_ns.response(401, 'Missing Authorization Header')
@wishlist_ns.response(404, 'Wishlist not found')
@wishlist_ns.param('pk', 'The wishlist identifier')
class Wishlist(Resource):
    """Show a single wishlist item and lets you delete them"""

    decorators = [jwt_required]

    @wishlist_ns.doc('get_wishlist')
    @wishlist_ns.response(200, 'Successful')
    def get(self, pk):
        """Fetch a given resource"""
        wishlist = wishlist_schema.dump(DAO.get(pk))
        return wishlist

    @wishlist_ns.doc('delete_wishlist')
    @wishlist_ns.response(204, 'Wishlist deleted')
    def delete(self, pk):
        """Delete a wishlist given its identifier"""
        DAO.delete(pk)
        return '', 204
