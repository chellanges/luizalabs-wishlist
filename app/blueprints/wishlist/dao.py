from flask import abort

from sqlalchemy.exc import IntegrityError

from app.ext.db import db
from app.utils.utils import apply_filter

from .models import Wishlist
from .schemas import wishlist_schema, wishlists_schema


class WishlistDAO(object):
    def list(self, args):
        query = db.session.query(Wishlist)
        if args:
            query = apply_filter(query, args.items())

        return wishlists_schema.dump(query)

    def get(self, pk):
        wishlist = Wishlist.query.get_or_404(pk)
        return wishlist

    def create(self, data):
        try:
            product_id = data['product_id']
            client_id = data['client_id']
            title = data['title']
            price = data['price']
            image = data['image']
            brand = data['brand']
            review_score = data.get('reviewScore')

            new_wishlist = Wishlist(product_id, title, price, image,
                                    client_id, brand, review_score)

            db.session.add(new_wishlist)
            db.session.commit()

            return wishlist_schema.dump(new_wishlist)
        except KeyError:
            abort(400)
        except IntegrityError:
            abort(409)
        except Exception:
            db.session.rollback()
            raise
        finally:
            db.session.close()

    def delete(self, pk):
        wishlist = self.get(pk)

        try:
            db.session.delete(wishlist)
            db.session.commit()
        except Exception:
            db.session.rollback()
            raise
        finally:
            db.session.close()

    def delete_by_client(self, client_id):
        query = db.session.query(Wishlist)

        try:
            query.filter_by(client_id=client_id).delete()
            db.session.commit()
        except Exception:
            db.session.rollback()
            raise
        finally:
            db.session.close()
