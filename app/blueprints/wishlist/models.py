from app.ext.db import db


class Wishlist(db.Model):
    __table_args__ = (
        db.UniqueConstraint('product_id', 'client_id'),
    )

    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.String(255), nullable=False)
    title = db.Column(db.String(255), nullable=False)
    price = db.Column(db.Float, nullable=False)
    image = db.Column(db.String(255), nullable=False)
    brand = db.Column(db.String(255))
    reviewScore = db.Column(db.Float)

    client_id = db.Column(db.Integer, db.ForeignKey('client.id'),
                          nullable=False)
    client = db.relationship('Client',
                             backref=db.backref('wishlist', lazy=True))

    def __init__(self, product_id, title, price, image, client_id,
                 brand=None, review_score=None):
        self.product_id = product_id
        self.title = title
        self.price = price
        self.image = image
        self.brand = brand
        self.reviewScore = review_score
        self.client_id = client_id

    def __repr__(self):
        return 'Wishlist(id={}, product_id={}, client_id={})'.format(
            self.id, self.product_id, self.client_id)
