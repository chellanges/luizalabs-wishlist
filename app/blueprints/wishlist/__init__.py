from flask import Blueprint
from flask_restplus import Namespace, fields

wishlist_bp = Blueprint('wishlist', __name__)

wishlist_ns = Namespace(path='/api/v1/wishlist',
                        name='wishlist',
                        description='Wishlist operations',)
wishlist_fields = wishlist_ns.model('Wishlist', {
    'client_id': fields.Integer('The client id'),
    'product_id': fields.String('The product id')
})
wishlist_list_md = wishlist_ns.model('WishlistList', {
    'data': fields.List(fields.Nested(wishlist_fields)),
})

from . import views  # noqa
