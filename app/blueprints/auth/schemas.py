from app.ext.marshmallow import ma

from .models import User


class UserSchema(ma.ModelSchema):
    class Meta:
        model = User
        fields = ('id', 'username', )

    _links = ma.Hyperlinks({
        'self': ma.URLFor('auth_user', pk='<id>'),
        'collection': ma.URLFor('auth_user_list'),
    })


user_schema = UserSchema()
users_schema = UserSchema(many=True)
