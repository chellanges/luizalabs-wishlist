from flask import Blueprint
from flask_restplus import Namespace, fields

auth_bp = Blueprint('auth', __name__)

auth_ns = Namespace(path='/api/v1/auth',
                    name='auth',
                    description='Auth operations',)
user_md = auth_ns.model('User', {
    'username': fields.String('The user username'),
    'password': fields.String('The user password')
})

from . import views  # noqa
