from flask import abort
from passlib.hash import pbkdf2_sha256 as sha256

from sqlalchemy.exc import IntegrityError

from app.ext.db import db
from app.utils.utils import apply_filter

from .models import User, RevokedToken
from .schemas import user_schema, users_schema


class UserDAO(object):
    def list(self, args):
        query = db.session.query(User)
        if args:
            query = apply_filter(query, args.items())

        return users_schema.dump(query)

    def get(self, pk):
        user = User.query.get_or_404(pk)
        return user

    def get_by_username(self, username):
        user = User.query.filter_by(username=username).first()
        return user

    def create(self, data):
        try:
            username = data['username']
            password = sha256.hash(data['password'])
            new_user = User(username, password)

            db.session.add(new_user)
            db.session.commit()

            return user_schema.dump(new_user)
        except KeyError:
            abort(400)
        except IntegrityError:
            abort(409)
        except Exception:
            db.session.rollback()
            raise
        finally:
            db.session.close()

    def delete(self, pk):
        user = self.get(pk)

        try:
            db.session.delete(user)
            db.session.commit()
        except Exception:
            db.session.rollback()
            raise
        finally:
            db.session.close()


class RevokedTokenDAO(object):
    def get_by_jti(self, jti):
        result = RevokedToken.query.filter_by(jti=jti)
        return result

    def create(self, data):
        try:
            new_revoked = RevokedToken(data['jti'])

            db.session.add(new_revoked)
            db.session.commit()

            return user_schema.dump(new_revoked)
        except KeyError:
            abort(400)
        except IntegrityError:
            abort(409)
        except Exception:
            db.session.rollback()
            raise
        finally:
            db.session.close()
