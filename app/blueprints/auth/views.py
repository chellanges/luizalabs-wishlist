from flask import abort, current_app as app, jsonify, request, url_for
from flask_jwt_extended import create_access_token, create_refresh_token
from flask_jwt_extended import jwt_required, jwt_refresh_token_required
from flask_jwt_extended import get_jwt_identity, get_raw_jwt
from flask_restplus import Resource
from passlib.hash import pbkdf2_sha256 as sha256

from app.ext.jwt import jwt
from app.utils.utils import get_paginated_list

from . import auth_ns, user_md
from .dao import UserDAO, RevokedTokenDAO

DAO = UserDAO()
revokedDAO = RevokedTokenDAO()


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    result = revokedDAO.get_by_jti(jti)
    return jti in result


@auth_ns.route('/users')
@auth_ns.doc(security='apiKey')
class UserList(Resource):
    """Shows a list of all users, and lets you POST to add new user"""

    @auth_ns.doc('list_users')
    @auth_ns.param('offset', 'Offset items')
    @auth_ns.param('limit', 'Limit items')
    @auth_ns.response(200, 'Successful')
    @auth_ns.response(401, 'Missing Authorization Header')
    @jwt_required
    def get(self):
        """List all users"""
        args = request.args.to_dict(flat=True)
        start = int(args.pop('offset', 1))
        limit = int(args.pop('limit', app.config['ITEMS_PER_PAGE']))
        result = DAO.list(args)

        return jsonify(get_paginated_list(result, url_for('auth_user_list'),
                                          start, limit))

    @auth_ns.doc('creste_user', security=None)
    @auth_ns.expect(user_md)
    @auth_ns.response(201, 'User created')
    @auth_ns.response(400, 'Missing or incorrect fields')
    @auth_ns.response(409, 'Username already exists')
    def post(self):
        """Create a new user"""
        return DAO.create(auth_ns.payload), 201


@auth_ns.route('/users/<int:pk>')
@auth_ns.doc(security='apiKey')
@auth_ns.response(401, 'Missing Authorization Header')
@auth_ns.response(404, 'User not found')
@auth_ns.param('pk', 'The user identifier')
class User(Resource):
    """Show a single user item and lets you delete them"""

    decorators = [jwt_required]

    @auth_ns.doc('delete_user')
    @auth_ns.response(204, 'User deleted')
    def delete(self, pk):
        """Delete a user given its identifier"""
        DAO.delete(pk)
        return '', 204


@auth_ns.route('/login')
@auth_ns.doc(security=None)
class Login(Resource):
    """Lets you POST to login"""

    @auth_ns.doc('login')
    @auth_ns.expect(user_md)
    @auth_ns.response(200, 'Login done')
    @auth_ns.response(400, 'Missing or incorrect fields')
    @auth_ns.response(401, 'Wrong credentials')
    @auth_ns.response(404, 'User doesn\'t exist')
    def post(self):
        """Login"""
        try:
            username = auth_ns.payload['username']
            password = auth_ns.payload['password']

            current_user = DAO.get_by_username(username)
            if not current_user:
                abort(404)

            if sha256.verify(password, current_user.password):
                access_token = create_access_token(identity=username)
                refresh_token = create_refresh_token(identity=username)
                return {
                    'msg': 'Logged in as {}'.format(current_user.username),
                    'access_token': access_token,
                    'refresh_token': refresh_token
                }
            else:
                abort(401)

        except KeyError:
            abort(400)


@auth_ns.route('/logout')
@auth_ns.doc(security='apiKey')
@auth_ns.response(401, 'Missing Authorization Header')
class Logout(Resource):
    """Lets you POST to revoked token"""

    decorators = [jwt_required]

    @auth_ns.doc('logout')
    @auth_ns.response(200, 'Access token has been revoked')
    def post(self):
        """Logout"""
        revokedDAO.create(get_raw_jwt())
        return {}, 200


@auth_ns.route('/logout-refresh')
@auth_ns.doc(security='apiKey')
@auth_ns.response(401, 'Missing Authorization Header')
class LogoutRefresh(Resource):
    """Lets you POST to revoked refresh token"""

    decorators = [jwt_refresh_token_required]

    @auth_ns.doc('logout_refresh')
    @auth_ns.response(200, 'Refresh token has been revoked')
    def post(self):
        """Logout Refresh"""
        revokedDAO.create(get_raw_jwt())
        return {}


@auth_ns.route('/token-refresh')
@auth_ns.doc(security='apiKey')
@auth_ns.response(401, 'Missing Authorization Header')
class TokenRefresh(Resource):
    """Lets you POST to refresh token"""

    decorators = [jwt_refresh_token_required]

    @auth_ns.doc('token_refresh')
    @auth_ns.response(200, 'Access token renewed')
    def post(self):
        """Token Refresh"""
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        return {'access_token': access_token}
