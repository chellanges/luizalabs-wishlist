from flask import jsonify

from app.jobs.tasks import add_product

from . import tasks


@tasks.route('/status/<task_id>', methods=['GET'])
def status(task_id):
    task = add_product.AsyncResult(task_id)
    response = {
        'state': task.state,
        'retries': task.retries if task.retries is not None else 0,
    }

    if task.failed():
        response['result'] = str(task.result)
        response['traceback'] = task.traceback
    elif task.successful():
        response['result'] = task.result

    return jsonify(response)
