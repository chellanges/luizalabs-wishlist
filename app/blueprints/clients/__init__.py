from flask import Blueprint
from flask_restplus import Namespace, fields

clients_bp = Blueprint('clients', __name__)

clients_ns = Namespace(path='/api/v1/clients',
                       name='clients',
                       description='Clients operations',)
client_md = clients_ns.model('Client', {
    'name': fields.String('The client name'),
    'email': fields.String('The client email')
})

from . import views  # noqa
