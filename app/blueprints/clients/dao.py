from flask import abort

from sqlalchemy.exc import IntegrityError

from app.ext.db import db
from app.utils.utils import apply_filter

from .models import Client
from .schemas import client_schema, clients_schema


class ClientDAO(object):
    def list(self, args):
        query = db.session.query(Client)
        if args:
            query = apply_filter(query, args.items())

        return clients_schema.dump(query)

    def get(self, pk):
        client = Client.query.get_or_404(pk)
        return client

    def create(self, data):
        try:
            new_client = Client(data['name'], data['email'])

            db.session.add(new_client)
            db.session.commit()

            return client_schema.dump(new_client)
        except KeyError:
            abort(400)
        except IntegrityError:
            abort(409)
        except Exception:
            db.session.rollback()
            raise
        finally:
            db.session.close()

    def update(self, pk, data):
        client = self.get(pk)
        try:
            client.name = data['name']
            client.email = data['email']

            db.session.commit()

            return client_schema.dump(client)
        except KeyError:
            abort(400)
        except Exception:
            db.session.rollback()
            raise
        finally:
            db.session.close()

    def delete(self, pk):
        client = self.get(pk)

        try:
            db.session.delete(client)
            db.session.commit()
        except Exception:
            db.session.rollback()
            raise
        finally:
            db.session.close()
