from flask import current_app as app, jsonify, request, url_for
from flask_jwt_extended import jwt_required
from flask_restplus import Resource

from app.utils.utils import get_paginated_list

from . import clients_ns, client_md
from .dao import ClientDAO
from .schemas import client_schema

DAO = ClientDAO()


@clients_ns.route('/')
@clients_ns.doc(security='apiKey')
@clients_ns.response(401, 'Missing Authorization Header')
class ClientList(Resource):
    """Shows a list of all clients, and lets you POST to add new client"""

    decorators = [jwt_required]

    @clients_ns.doc('list_clients')
    @clients_ns.param('offset', 'Offset items')
    @clients_ns.param('limit', 'Limit items')
    @clients_ns.response(200, 'Successful')
    def get(self):
        """List all clients"""
        args = request.args.to_dict(flat=True)
        start = int(args.pop('offset', 1))
        limit = int(args.pop('limit', app.config['ITEMS_PER_PAGE']))
        result = DAO.list(args)
        result_paginated = get_paginated_list(
            result, url_for('clients_client_list'), start, limit)

        return jsonify(result_paginated)

    @clients_ns.doc('creste_client')
    @clients_ns.expect(client_md)
    @clients_ns.response(201, 'Client created')
    @clients_ns.response(400, 'Missing or incorrect fields')
    @clients_ns.response(409, 'Email already exists')
    def post(self):
        """Create a new client"""
        return DAO.create(clients_ns.payload), 201


@clients_ns.route('/<int:pk>')
@clients_ns.doc(security='apiKey')
@clients_ns.response(401, 'Missing Authorization Header')
@clients_ns.response(404, 'Client not found')
@clients_ns.param('pk', 'The client identifier')
class Client(Resource):
    """Show a single client item and lets you delete them"""

    decorators = [jwt_required]

    @clients_ns.doc('get_client')
    @clients_ns.response(200, 'Successful')
    def get(self, pk):
        """Fetch a given resource"""
        client = client_schema.dump(DAO.get(pk))
        return client

    @clients_ns.doc('update_client')
    @clients_ns.expect(client_md)
    @clients_ns.response(200, 'Successful')
    def put(self, pk):
        """Update a client given its identifier"""
        return DAO.update(pk, clients_ns.payload)

    @clients_ns.doc('delete_client')
    @clients_ns.response(204, 'Client deleted')
    def delete(self, pk):
        """Delete a client given its identifier"""
        DAO.delete(pk)
        return '', 204
