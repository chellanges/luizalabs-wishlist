from app.ext.marshmallow import ma

from .models import Client


class ClientSchema(ma.ModelSchema):
    class Meta:
        model = Client

    _links = ma.Hyperlinks({
        'self': ma.URLFor('clients_client', pk='<id>'),
        'collection': ma.URLFor('clients_client_list'),
        'wishlist': ma.URLFor('wishlist_wishlist_list', client_id='<id>'),
    })


client_schema = ClientSchema()
clients_schema = ClientSchema(many=True)
