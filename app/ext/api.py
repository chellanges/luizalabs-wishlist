from flask_restplus import Api

authorizations = {
    'apiKey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    },
}

api = Api(
    title='Wishlist',
    version='1.0',
    authorizations=authorizations,
    description='Magalu - Managing our customers and their favorite products'
)


def init_app(app):
    api.init_app(app)


def add_namespace(ns):
    api.add_namespace(ns)
