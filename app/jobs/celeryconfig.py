# Broker and Backend
broker_url = 'redis://localhost:6379/1'
result_backend = 'redis://localhost:6379/0'

# import
imports = 'app.jobs.tasks'

# Timezone
timezone = 'America/Recife'
enable_utc = True

task_serializer = 'pickle'
result_serializer = 'pickle'
accept_content = ['pickle']
