import requests

from flask import current_app as app

from requests.adapters import HTTPAdapter
from requests.exceptions import HTTPError

from app import celery
from app.blueprints.wishlist.dao import WishlistDAO

DAO = WishlistDAO()


@celery.task(name='tasks.add_product_list')
def add_product_list(payload):
    for item in payload['data']:
        client_id = item['client_id']
        product_id = item['product_id']
        add_product.apply_async([client_id, product_id])

    return None


@celery.task(bind=True, name='tasks.add_product',
             retry_limit=5, default_retry_delay=15)
def add_product(self, client_id, product_id):
    url = '{}/{}'.format(app.config['PRODUCT_URL'], product_id)
    product_adapter = HTTPAdapter(max_retries=3)
    session = requests.Session()
    session.mount(url, product_adapter)

    response = session.get(url, timeout=(10, 5))

    try:
        response.raise_for_status()
    except HTTPError as e:
        if e.response.status_code == 503:
            self.retry(exc=e)
        else:
            raise e

    data = response.json()
    data['client_id'] = client_id
    data['product_id'] = product_id

    return DAO.create(data)


@celery.task(name='tasks.delete_wishlist')
def delete_wishlist(client_id):
    DAO.delete_by_client(client_id)
