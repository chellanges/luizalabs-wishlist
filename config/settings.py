from prettyconf import config as conf


class Config:
    SECRET_KEY = conf('SECRET_KEY', default='guess')
    JWT_SECRET_KEY = conf('JWT_SECRET_KEY', default='guess-jwt')
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']

    @staticmethod
    def init_app(app):
        pass


class ProductionConfig(Config):
    ENV = 'production'
    FLASK_ENV = 'production'
    FLASK_APP = conf('FLASK_APP', default='wishlist')
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    COMPOSE_PROJECT_NAME = conf('COMPOSE_PROJECT_NAME', default='wishlist')
    DEBUG = conf('DEBUG', default=True, cast=bool)
    DATABASE_LOCATION = conf('DATABASE_LOCATION', default='sqlite://')
    DATABASE_NAME = conf('DATABASE_NAME', default=':memory:')
    SQLALCHEMY_TRACK_MODIFICATIONS = conf('SQLALCHEMY_TRACK_MODIFICATIONS', default=False, cast=bool)
    SQLALCHEMY_DATABASE_URI = f'{DATABASE_LOCATION}/{DATABASE_NAME}'
    PRODUCT_URL = conf('PRODUCT_URL', default='http://challenge-api.luizalabs.com/api/product')
    ITEMS_PER_PAGE = conf('ITEMS_PER_PAGE', default=20, cast=int)
    C_FORCE_ROOT = conf('C_FORCE_ROOT', default=False, cast=bool)
    CELERY_MAX_CACHED_RESULTS = conf('CELERY_MAX_CACHED_RESULTS', default=-1, cast=int)
    CELERY_BROKER_URL = conf('CELERY_BROKER_URL', default='redis://localhost:6379/1')
    CELERY_RESULT_BACKEND = conf('CELERY_RESULT_BACKEND', default='redis://localhost:6379/0')
    PYTHONDONTWRITEBYTECODE = conf('PYTHONDONTWRITEBYTECODE', default=False, cast=bool)
    SERVER_NAME = conf('SERVER_NAME', default='localhost:8000')
    WEB_RELOAD = conf('WEB_RELOAD', default=True, cast=bool)
    WEB_BIND = conf('WEB_BIND', default='0.0.0.0:8000')
    WEB_CONCURRENCY = conf('WEB_CONCURRENCY', default=1, cast=int)
    PYTHON_MAX_THREADS = conf('PYTHON_MAX_THREADS', default=1, cast=int)
    DOCKER_RESTART_POLICY = conf('DOCKER_RESTART_POLICY', default='no')
    DOCKER_HEALTHCHECK_TEST = conf('DOCKER_HEALTHCHECK_TEST', default='/bin/true')
    DOCKER_STOP_GRACE_PERIOD = conf('DOCKER_STOP_GRACE_PERIOD', default='3s')
    DOCKER_WEB_PORT = conf('DOCKER_WEB_PORT', default='8000')
    DOCKER_WEB_VOLUME = conf('DOCKER_WEB_VOLUME', default='.:/app')
    DOCKER_POSTGRES_PORT = conf('DOCKER_POSTGRES_PORT', default='127.0.0.1:5432:5432')


class DevelopmentConfig(Config):
    ENV = 'development'
    FLASK_ENV = 'development'
    FLASK_APP = conf('FLASK_APP', default='wishlist')
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    COMPOSE_PROJECT_NAME = conf('COMPOSE_PROJECT_NAME', default='wishlist')
    DEBUG = conf('DEBUG', default=True, cast=bool)
    DATABASE_LOCATION = conf('DATABASE_LOCATION', default='sqlite://')
    DATABASE_NAME = conf('DATABASE_NAME', default=':memory:')
    SQLALCHEMY_TRACK_MODIFICATIONS = conf('SQLALCHEMY_TRACK_MODIFICATIONS', default=False, cast=bool)
    SQLALCHEMY_DATABASE_URI = f'{DATABASE_LOCATION}/{DATABASE_NAME}'
    PRODUCT_URL = conf('PRODUCT_URL', default='http://challenge-api.luizalabs.com/api/product')
    ITEMS_PER_PAGE = conf('ITEMS_PER_PAGE', default=20, cast=int)
    C_FORCE_ROOT = conf('C_FORCE_ROOT', default=False, cast=bool)
    CELERY_MAX_CACHED_RESULTS = conf('CELERY_MAX_CACHED_RESULTS', default=-1, cast=int)
    CELERY_BROKER_URL = conf('CELERY_BROKER_URL', default='redis://localhost:6379/1')
    CELERY_RESULT_BACKEND = conf('CELERY_RESULT_BACKEND', default='redis://localhost:6379/0')
    PYTHONDONTWRITEBYTECODE = conf('PYTHONDONTWRITEBYTECODE', default=False, cast=bool)
    SERVER_NAME = conf('SERVER_NAME', default='localhost:8000')
    WEB_RELOAD = conf('WEB_RELOAD', default=True, cast=bool)
    WEB_BIND = conf('WEB_BIND', default='0.0.0.0:8000')
    WEB_CONCURRENCY = conf('WEB_CONCURRENCY', default=1, cast=int)
    PYTHON_MAX_THREADS = conf('PYTHON_MAX_THREADS', default=1, cast=int)
    DOCKER_RESTART_POLICY = conf('DOCKER_RESTART_POLICY', default='no')
    DOCKER_HEALTHCHECK_TEST = conf('DOCKER_HEALTHCHECK_TEST', default='/bin/true')
    DOCKER_STOP_GRACE_PERIOD = conf('DOCKER_STOP_GRACE_PERIOD', default='3s')
    DOCKER_WEB_PORT = conf('DOCKER_WEB_PORT', default='8000')
    DOCKER_WEB_VOLUME = conf('DOCKER_WEB_VOLUME', default='.:/app')
    DOCKER_POSTGRES_PORT = conf('DOCKER_POSTGRES_PORT', default='127.0.0.1:5432:5432')


class TestingConfig(Config):
    ENV = 'testing'
    DEBUG = True
    TESTING = True
    BCRYPT_LOG_ROUNDS = 4
    WTF_CSRF_ENABLED = False
    FLASK_ENV = 'testing'
    FLASK_APP = conf('FLASK_APP', default='wishlist')
    COMPOSE_PROJECT_NAME = conf('COMPOSE_PROJECT_NAME', default='wishlist')
    DATABASE_LOCATION = 'sqlite://'
    DATABASE_NAME = ':memory:'
    SQLALCHEMY_DATABASE_URI = f'{DATABASE_LOCATION}/{DATABASE_NAME}'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    PRODUCT_URL = conf('PRODUCT_URL', default='http://challenge-api.luizalabs.com/api/product')
    ITEMS_PER_PAGE = conf('ITEMS_PER_PAGE', default=20, cast=int)
    C_FORCE_ROOT = conf('C_FORCE_ROOT', default=False, cast=bool)
    CELERY_MAX_CACHED_RESULTS = conf('CELERY_MAX_CACHED_RESULTS', default=-1, cast=int)
    CELERY_BROKER_URL = conf('CELERY_BROKER_URL', default='redis://localhost:6379/1')
    CELERY_RESULT_BACKEND = conf('CELERY_RESULT_BACKEND', default='redis://localhost:6379/0')
    PYTHONDONTWRITEBYTECODE = conf('PYTHONDONTWRITEBYTECODE', default=False, cast=bool)
    SERVER_NAME = conf('SERVER_NAME', default='localhost:8000')
    WEB_RELOAD = conf('WEB_RELOAD', default=True, cast=bool)
    WEB_BIND = conf('WEB_BIND', default='0.0.0.0:8000')
    WEB_CONCURRENCY = conf('WEB_CONCURRENCY', default=1, cast=int)
    PYTHON_MAX_THREADS = conf('PYTHON_MAX_THREADS', default=1, cast=int)
    DOCKER_RESTART_POLICY = conf('DOCKER_RESTART_POLICY', default='no')
    DOCKER_HEALTHCHECK_TEST = conf('DOCKER_HEALTHCHECK_TEST', default='/bin/true')
    DOCKER_STOP_GRACE_PERIOD = conf('DOCKER_STOP_GRACE_PERIOD', default='3s')
    DOCKER_WEB_PORT = conf('DOCKER_WEB_PORT', default='8000')
    DOCKER_WEB_VOLUME = conf('DOCKER_WEB_VOLUME', default='.:/app')
    DOCKER_POSTGRES_PORT = conf('DOCKER_POSTGRES_PORT', default='127.0.0.1:5432:5432')


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
}
