# Luiza Labs - Wishlist

Magalu - Managing our customers and their favorite products

## Description

API Rest to manage our customers wishlist products.  

## Usage

To run this app you'll need:

* Python 3.7+ (It should work with older Python 3 version).
* PIPENV
* Docker

There is one easy ways to run and test this app.

### Locally

1. Clone this repository.
2. Install requirements: `pipenv install`
3. Create a `.env` file. Use `contrib/env-sample` as a example.
4. Run the following commands to create some containers:
* `docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -p 5432:5432 -v pgdata:/var/lib/postgresql/data -d postgres`
* `docker run --name some-redis -p 6379:6379 -d redis`
5. Create the database: `docker exec -it some-postgres psql -U postgres -c "CREATE DATABASE wishlistdb;"`
6. Apply the migrations: `python manager.py db-upgrade`
7. Run the application: `python manager.py runserver`
8. Run the following commands on <b>different terminals</b> for worker and monitor creation:
* `celery worker -B -A celery_worker.celery -l=info`
* `flower -A celery_worker.celery --port=5555`

Remember to authenticate and place the word Bearer before the access token, for example: `Bearer <access-token>`

The API documentation can be checked at the application root, for example: http://localhost:8000/  
Monitoring of background tasks with flower: http://localhost:5555/

## Testing

To test, just run `python manager.py tests`.

## Implementation Details

About the development environment, I'm developing this project using a Notebook,
running openSUSE Tumbleweed. I have written this text and code entirely in PyCharm.

To develop this app I added some extra dependencies:

* *[flask](https://palletsprojects.com/p/flask/):* Flask is a lightweight WSGI web application framework. It is designed to make getting started quick and easy.
* *[pytest](https://docs.pytest.org/en/latest/):* The pytest framework makes it easy to write small tests, yet scales to support complex functional testing for applications and libraries.
* *[pytest-cov](https://pypi.org/project/pytest-cov/):* Pytest plugin for measuring coverage.
* *[prettyconf](https://github.com/osantana/prettyconf):* Pretty Conf is a Python library created to make easy the separation of configuration and code following the recomendations of 12 Factor's topic about configs.
* *[flask-sqlalchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/):* Is an extension for Flask that adds support for SQLAlchemy to your application.
* *[flask-migrate](https://flask-migrate.readthedocs.io/en/latest/):* is an extension that handles SQLAlchemy database migrations for Flask applications using Alembic.
* *[celery](http://www.celeryproject.org/):* Is an asynchronous task queue/job queue based on distributed message passing.
* *[redis](https://redis.io/):* is an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker.
* *[flower](https://flower.readthedocs.io/en/latest/):* is a web based tool for monitoring and administrating Celery clusters.
* *[psycopg2](https://pypi.org/project/psycopg2/):* Python-PostgreSQL Database Adapter
* *[flask-marshmallow](https://flask-marshmallow.readthedocs.io/en/latest/):* Is a thin integration layer for Flask and marshmallow that adds additional features to marshmallow, including URL and Hyperlinks fields for HATEOAS-ready APIs.
* *[marshmallow-sqlalchemy](https://marshmallow-sqlalchemy.readthedocs.io/en/latest/):* SQLAlchemy integration with the marshmallow (de)serialization library.
* *[requests](https://2.python-requests.org/en/master/):* Is an elegant and simple HTTP library for Python, built for human beings.
* *[flask-jwt-extended](https://flask-jwt-extended.readthedocs.io/en/latest/):* JWT integration with the flask application.
* *[passlib](https://passlib.readthedocs.io/en/stable/):* Is a password hashing library for Python, which provides cross-platform implementations of over 30 password hashing algorithms, as well as a framework for managing existing password hashes.
* *[gunicorn](https://gunicorn.org/):* Is a Python WSGI HTTP Server for UNIX.
* *[flask-restplus](https://flask-restplus.readthedocs.io/en/stable/):* Is an extension for Flask that adds support for quickly building REST APIs.
