#!/bin/bash

# Apply database migrations
echo "Apply database migrations"
python manager.py db-init
python manager.py db-migrate
python manager.py db-upgrade

# Start server
echo "Starting server"
gunicorn -c python:config.gunicorn app:create_app\(\)
